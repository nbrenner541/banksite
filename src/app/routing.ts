import { RouterModule, Routes } from '@angular/router';
import { LanderComponent } from './lander/lander.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { ThankyouComponent } from './thankyou/thankyou.component';

const APP_ROUTES: Routes = [
  { path: 'checkout', component: CheckoutComponent },
  { path: 'thankyou', component: ThankyouComponent },
  { path: 'lander', component: LanderComponent },
  { path: '', component: LanderComponent }
];

export const routing = RouterModule.forRoot(APP_ROUTES);
